
# AIDE

This is an IDE-driver for the Amiga with support for LBA48 and ATAPI written in 68k assembler.

![PCB](https://gitlab.com/MHeinrichs/AIDE/raw/master/Pics/PCB.jpg)

It uses a very simple PCB and can be made autobooting for kick2.0 and above, if a custom rom is made including the ide.device and autoboot.device and excluding the scsi.device.

However, both devices can be made resetsafe with aboot floppy and using [LoadModule](http://aminet.net/package/util/boot/LoadModule) from the Aminet.

**DISCLAIMER!!!!**  
**THIS IS A HOBBYIST PROJECT! I'M NOT RESPONSIBLE IF IT DOESN'T WORK OR DESTROYS YOUR VALUABLE DATA!**

## License
1. Do whatever you want with this, if you do it for private use and mention this work as origin. 
2. If you want to make money out of this work, ask me first!
3. If you copy parts of this work to your project, mention the source.
4. If you use parts of this work for closed source, burn in hell and live in fear!
