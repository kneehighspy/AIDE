	SECTION   driver,CODE
	include "exec/types.i"
	include "exec/nodes.i"
	include "exec/lists.i"
	include "exec/libraries.i"
	include "exec/devices.i"
	include "exec/io.i"
	include "exec/alerts.i"
	include "exec/initializers.i"
	include "exec/memory.i"
	include "exec/resident.i"
	include "exec/ables.i"
	include "exec/errors.i"
	include "exec/tasks.i"
	include "devices/scsidisk.i"
	include "devices/hardblocks.i"
	include "libraries/expansion.i"
	include "libraries/configvars.i"
	include "libraries/configregs.i"
	include "libraries/expansionbase.i"
	include "libraries/filehandler.i"
	;Note that the next ASSIGNs ending with : need to be assigned
	;outside of this assembly source file, eg. in compilation scripts.
	;These are AmigaDos "links" to some certain file.
	include "lib/asmsupp.i";Various helper macros made by Commodore
	include "lib/mydev.i"  ;select name etc, of the device
	;include "lib/myscsi.i" ;
	include "lib/ata.i"    ;ATA commands and other ATA codes
	include "lib/atid.i"   ;This include has the macros which
	      ;are used to access a particular
	      ;implementation of an Amiga to ATA 
	      ;hardware interface, such as an A500 
	      ;side slot interface or a Parallel port
	      ;interface.
		
	
;This routine finds if a drive is attached:
; first it issues a write to a register
; second it reads the status and checks "busy" and "not ready"
; if it is busy or not ready it might be there: wait 5 seconds for the drive to get ready
; if it is busy and nor ready it is a empty bus->no HW
; if it is neither busy nor "not ready" we have to read/write some registers to check if it is there
;d0 holds the unit number
	Public FindDrive
FindDrive
	movem.l  d1/d2,-(sp)	 
	WATABYTE d0,TF_DRIVE_HEAD            ; select the drive
	move.l   #TIMEOUT,d1                 ; wait timeout*sec 
check_status:
	WATABYTE #TESTBYTE1,TF_SECTOR_COUNT ; write first testbyte
	RATABYTE	TF_ALTERNATE_STATUS,d0                ; get status
	and.b    #BSY+DRDY,d0                ; eval Busy and DRDY  
	beq      test_registers              ; none: test registers
	cmp.b    #BSY+DRDY,d0                ; both: impossible
	beq      bad_return_from_find
	and.b    #BSY,d0
	beq      test_registers              ;Not busy->test registers
	;bad busy wait
	move.l   #500000,d0	    
wait_loop:
	tst.b    $bfe301 ;slow CIA access cycle takes 12-20 7MHz clocks: 1.7us - 2.8us
	subq.l	 #1,D0
	bne.s    wait_loop
	dbra     d1,check_status             ; check again
	bra      bad_return_from_find        ; timeout reached: not drive here   
test_registers:
	; Write some Registers and read thew value back
	WATABYTE #TESTBYTE1,TF_SECTOR_COUNT
	 RATABYTE	TF_SECTOR_COUNT,d0
	 cmp.b	#TESTBYTE1,d0
	bne  	bad_return_from_find
	WATABYTE #TESTBYTE2,TF_SECTOR_COUNT
	 RATABYTE	TF_SECTOR_COUNT,d0
	 cmp.b	#TESTBYTE2,d0
	bne  	bad_return_from_find
	WATABYTE #TESTBYTE3,TF_SECTOR_COUNT
	 RATABYTE	TF_SECTOR_COUNT,d0
	 cmp.b	#TESTBYTE3,d0
	bne  	bad_return_from_find
	; we found a drive!
good_return_from_find:
	MOVE.l   #0,d0   
	bra.s    return_from_find
bad_return_from_find:
	MOVE.l   #-1,d0   
return_from_find:
	movem.l  (sp)+,d1/d2
	rts

